

<header class="head_1st">
    <h2 class="logo"><a href="/">V-Kart</a></h2>
    <div class="search-container">
        <form action="">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <div class="sidebar-social">
        <ul>
         
            @if(Auth::guest())
                  
                        <li class="nav-item">
                         <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-sign-in"></i>{{ __('Sign In') }}</a>
                            </li>
                      
                    
             @else
                      <li class="nav-item ">
                                <a  href="#" ><i class="fa fa-user"></i>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                               <li class="nav-item">
                        <a class="nav-link" href="#" title="Cart" ><i class="fa fa-shopping-bag"></i>
                             <span>Cart</span>
                        </a>
                        </li>
                            <li class="nav-item ">
                              
                                    <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"
                                                     ></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
            @endif
            

             
        </ul>
    </div>
</header>