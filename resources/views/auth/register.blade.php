
      <form action="{{ route('register') }}"  method="POST" id="signup"  class="input-group">
        @csrf 
             <input id="name" type="text" class="input-form @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

              @error('name')
                    <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                        </span>
              @enderror

             <div class="col-md-6">
                                <input id="email" type="email" class="input-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Id">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
           <input id="password" type="password" class="input-form @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
             <input id="password-confirm" type="password" class="input-form" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password">
            <input type="checkbox" class="check-box" required><span>I agree to the terms & conditions</span>
            
             <button type="submit" class="submit-btns" name="submit">
                                    {{ __('Sign Up') }}
              </button>
        </form>

