@extends('layouts.app')

@section('content')
<!-- <div class="banner-img"></div> -->
<div class="hero">
    <div id="user"><i class="fa fa-user-circle"  aria-hidden="true"></i></div>
    <div class="from-box">
        <div class="button-box">
            <div id="btns"></div>
            <button type="button" class="toggles-btn" onclick="signin()">Sign In</button>
            <button type="button" class="toggles-btn" onclick="signup()">Sign Up</button>
        </div>
        <div class="social-icons">
            <a href="/login/facebook"><i class="fa fa-facebook"></i></a>
            <a href="/login/google"><i class="fa fa-google"></i></a>
            <i class="fa fa-twitter"></i>
        </div>
        <form  action="{{ route('login') }}" method="POST" id="signin" class="input-group">
            @csrf
           <input type="email" class="input-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Id">

             @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
              @enderror
            <input  type="password" class="input-form @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
             @error('password')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                         </span>
             @enderror
             <input class="form-check-input check-box" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><span>Remember Password</span>
            
            <button type="submit" class="submit-btns" name="submit">
                                    {{ __('Sign In') }}
                                </button>
            <br>
             @if (Route::has('password.request'))
             <a class="btn btn-link" href="{{ route('password.request') }}">
              {{ __('Forgot Your Password?') }}
              </a>
             @endif
        </form>
  @include('auth.register')
    </div>
<script type="text/javascript">
    var x = document.getElementById("signin");
    var y = document.getElementById("signup");
    var z = document.getElementById("btns");

    function signup() {
        x.style.left = "-400px";
        y.style.left = "50px";
        z.style.left = "110px";
    } 

    function signin() {
        x.style.left = "50px";
        y.style.left = "450px";
        z.style.left = "0px";
    }
</script>    
@endsection
