@extends('Admin.layouts.master')

@section('content')
		<div class="container jumbotron">

			@foreach($products as $product) 
			@if(count($product->getimages)==0)
					<p class="alert-danger"> {{'Image is required'}} </p> 
				@endif
				 <div class="row">
				 	@foreach($product->getimages as $images)

				<div class="col-2">
				<img src="{{asset('storage/images/'.$images->product_image)}}" style="width: 200px" alt="Noimage" class="img-thumbnail">
				<div class="row ml-2 mt-2">
					<button class="btn-sm btn btn-danger"><a class="text-white" href="/deleteimage/{{$images->id}}">Delete</a></button>
				</div>
				</div>
				@endforeach
				</div>
				
				
				
				<form action="/add_more_images/{{$product->id}}" enctype="multipart/form-data" method="post">
			@csrf 
					<br><h4>{{$product->product_name}}</h4><br>
				Product photos (can attach more than one): <br>
				<input multiple="multiple" name="images[]" type="file">
				<br><br>
				<input type="submit" value="Upload">
				</form>
			@endforeach

			
		</div>
@endsection