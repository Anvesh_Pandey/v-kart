@extends('Admin.layouts.master')

@section('content')

	<div class="container mt-5">
<a href="/createsubcategory"><li class="btn btn-primary btn-sm mb-3" >Add Subcategory</li></a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Sub_Category Name</th>
      <th scope="col">Category Name</th>
      <th scope="col">Operations</th>
      <th scope="col">image</th>
    </tr>
  </thead>
  <tbody>
 
		  @foreach($categories as $category)
        @foreach($category->subcategories as $subcategory)
       <tr>
        <td>{{$subcategory->category_name}}</td>
        <td>{{$category->category_name}}</td>
        <td>
        <a href="/editsubcategory/{{$subcategory->id}}"><li class="btn btn-warning btn-sm">Edit Subcategory</li></a>
         <a href="/deletesubcategory/{{$subcategory->id}}"><li class="btn btn-danger btn-sm">Delete Subcategory</li></a>
        </td>
        <td><img src="{{asset('storage/images/'.$subcategory['category_image'])}}" style="width: 40px;"></td>
        </tr>
        @endforeach
    @endforeach
    
 
  </tbody>
</table>
</div>

@endsection 