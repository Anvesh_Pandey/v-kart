@extends('Admin.layouts.master')
@section('content')
<div class="container mt-5">
<form action="/updatesubcategory/{{$subcategory->id}}" enctype="multipart/form-data" method="POST">
  @csrf
  
  <div class="form-group">
   
    <input name="invisible"  type="hidden" method="PUT">
    <label for="subcategory_name">SubCategory Name</label>
    <input type="text" class="form-control" name="subcategory_name" id="subcategory_name"  placeholder="Enter category name" value="{{$subcategory['category_name']}}">
  </div>
  <div class="form-group">
    <label for="category_name">Category Name</label>

   <select id="category_name" name="category_id">

     @foreach($categories as $category)
           <option value="{{ $category->id }}" {{ $category->id == $subcategory->category_id ? 'selected' : '' }}>{{ $category->category_name }}</option>
     @endforeach
   </select>
  </div>
  <div class="form-group">
     <label for="subcategory_image">SubCategory_image</label>

    <input type="file" name="image" id="image" src="asset(storage\app\images\{{$subcategory['category_image']}})" placeholder="Upload">
     
  </div>
   
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

@endsection