@extends('Admin.layouts.master')

@section('content')

	<div class="container mt-5">
<a href="/add_product"><li class="btn btn-primary btn-sm mb-3" >Add Product</li></a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">product_name</th>
      <th scope="col">Sub_Category Name</th>
      <th scope="col">Category Name</th>
      <th scope="col">Operations</th>
      <th scope="col">image</th>
    </tr>
  </thead>
  <tbody>
@foreach($products as $product)     
        <tr>  
          <td>{{$product->product_name}}</td>
          
            <td>{{$product->getcategory['category_name']}}</td> 
          
            <td>{{$product->getcategory->parentcategory['category_name']}}</td>  
          
        <td>
        <a href="/editproduct/{{$product->id}}"><li class="btn btn-warning btn-sm">Edit product</li></a>
         <a href="/deleteproduct/{{$product->id}}"><li class="btn btn-danger btn-sm">Delete product</li></a>
        </td>
       
        <td>     
          <button class="btn btn-info btn-sm"><a class="text-white" href="/productimages/{{$product->id}}">Click for images</a></button>
        </td>  
      
        </tr>   
@endforeach
  </tbody>
</table>
</div>

@endsection 

