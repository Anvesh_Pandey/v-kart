@extends('Admin.layouts.master')
@section('content')
<div class="container mt-5">
<form action="/updateproduct/{{$product->id}}" enctype="multipart/form-data" method="POST">
  @csrf
  
  <div class="form-group">
    <label for="product_name">Product Name</label>
    <input type="text" class="form-control" name="product_name" id="product_name"  placeholder="Enter Product name" value="{{$product->product_name}}">
  </div>
 
   <div class="form-group">
    <label for="subcategory_name">SubCategory Name</label>

   <select id="subcategory_name" name="subcategory_id">
    @foreach($categories as $category)
          <optgroup label="{{$category->category_name}}"> 
           @foreach($category->subcategories as $subcategory)
             <option value="{{$subcategory->id}}" {{ $subcategory->id == $product->category_id ? 'selected' : '' }}>{{ $subcategory->category_name }}</option>
            @endforeach
          </optgroup>
    @endforeach
   </select>
  </div>
  <div class="form-group">
      <label for="price">price</label>
    <input type="number" class="form-control" name="price" id="price"  placeholder="Enter price/-" value="{{$product->price}}">
  </div>
  <div class="form-group">
      <label for="discounted_price">Discounted_price</label>
    <input type="number" class="form-control" name="discounted_price" id="discounted_price"  placeholder="Enter discounted_price/-" value="{{$product->discounted_price}}">
  </div>
  <div class="form-group">
      <label for="quantity">Quantity</label>
    <input type="number" class="form-control" name="quantity" id="quantity"  placeholder="Enter Quantity/-" value="{{$product->quantity}}">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" name="description" id="description"  placeholder="Enter Description" >{{$product->description}}</textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection