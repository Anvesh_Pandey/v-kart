@extends('Admin.layouts.master')
@section('content')
<div class="container mt-5">
<form action="/storeproduct" enctype="multipart/form-data" method="POST">
  @csrf
  
  <div class="form-group">
    <label for="product_name">Product Name</label>
    <input type="text" class="form-control" name="product_name" id="product_name"  placeholder="Enter Product name">
  </div>
 
   <div class="form-group">
    <label for="subcategory_name">SubCategory Name</label>

   <select id="subcategory_name" name="subcategory_id">
    @foreach($categories as $category)
          <optgroup label="{{$category->category_name}}"> 
           @foreach($category->subcategories as $subcategory)
            <option value="{{$subcategory->id}}">{{$subcategory->category_name}}</option>
            @endforeach
          </optgroup>
    @endforeach
   </select>
  </div>
  <div class="form-group">
      <label for="price">price</label>
    <input type="number" class="form-control" name="price" id="price"  placeholder="Enter price/-">
  </div>
  <div class="form-group">
      <label for="discounted_price">Discounted_price</label>
    <input type="number" class="form-control" name="discounted_price" id="discounted_price"  placeholder="Enter discounted_price/-">
  </div>
  <div class="form-group">
      <label for="quantity">Quantity</label>
    <input type="number" class="form-control" name="quantity" id="quantity"  placeholder="Enter Quantity/-">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" name="description" id="description"  placeholder="Enter Description"></textarea>
  </div>
  <div class="form-group">
     <label for="product_image">product_images</label>
    <input type="file" name="image"  id="image"  placeholder="Upload">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection