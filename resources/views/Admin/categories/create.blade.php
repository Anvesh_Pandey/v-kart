@extends('Admin.layouts.master')
@section('content')
<div class="container mt-5">
<form action="/storecategory" enctype="multipart/form-data" method="POST">
  @csrf
  
  <div class="form-group">
    <label for="category_name">Category Name</label>
    <input type="text" class="form-control" name="category_name" id="category_name"  placeholder="Enter category name">
  </div>
  <div class="form-group">
     <label for="category_image">Category_image</label>

    <input type="file" name="image" id="image"  placeholder="Upload">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection