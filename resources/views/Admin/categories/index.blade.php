@extends('Admin.layouts.master')

@section('content')
<div class="container mt-5">
<a href="/createcategory"><li class="btn btn-primary btn-sm mb-3" >Add category</li></a>
  
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Category Name</th>
      <th scope="col">Operations</th>
      <th scope="col">image</th>
    </tr>
  </thead>
  <tbody>
  	@if($categories->count()>0)
		@foreach($categories as $category)
    <tr>
      <td><a href="/subcategories/{{$category->id}}">{{$category->category_name}}</a></td>
      <td>
      	<a href="/editcategory/{{$category->id}}"><li class="btn btn-warning btn-sm">Edit category</li></a>
      	 <a href="/deletecategory/{{$category->id}}"><li class="btn btn-danger btn-sm">Delete category</li></a>
     </td>
     <td><img src="{{asset('storage/images/'.$category['category_image'])}}" style="width: 40px;"></td>
    </tr>
    @endforeach
    @endif
  </tbody>
</table>
<button class="btn btn-warning"><a href="/export" class="link-unstyled">Export</a></button>
</div>
@endsection

