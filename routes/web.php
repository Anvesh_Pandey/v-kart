<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index');                                     //Route for homepage
Route::get('contactus', 'WebController@contactus');                        //Route for Contact us page
Route::get('address', 'WebController@address');                            //Route for addresspage
Route::get('checkout', 'WebController@checkout');                          //Route for checkout page
Route::get('changepassword', 'WebController@changepassword');              //Route for changepassword page
Route::get('ordersuccess', 'WebController@ordersuccess');                  //Route for Order Success Page
Route::get('profile', 'WebController@profile');                            //Route for Proile page
Route::get('shop', 'WebController@shop');                                  //Route for Shop page
Route::get('product','WebController@product');                             //Route for Products Page
Route::get('cart', 'WebController@cart');                                 //Route for cart page 

Auth::routes();
Route::get('home', 'HomeController@index')->name('home');

Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/admin',function(){
		return view('Admin.index');
});
//category routes
Route::get('categories', 'CategoriesController@index');
Route::get('createcategory','CategoriesController@create');
Route::post('storecategory','CategoriesController@store');
Route::get('editcategory/{id}','CategoriesController@edit');
Route::post('updatecategory/{id}','CategoriesController@update');
Route::get('deletecategory/{id}','CategoriesController@destroy');
Route::get('export','CategoriesController@export');

//subcategory routes
Route::get('subcategories','SubcategoriesController@index');
Route::get('createsubcategory','SubCategoriesController@create');
Route::post('storesubcategory','SubcategoriesController@store');
Route::get('editsubcategory/{id}','SubcategoriesController@edit');
Route::post('updatesubcategory/{id}','SubcategoriesController@update');
Route::get('deletesubcategory/{id}','SubcategoriesController@destroy');

//product routes
Route::get('products','ProductsController@index');
Route::get('add_product','ProductsController@create');
Route::post('storeproduct','ProductsController@store');
Route::get('editproduct/{id}','ProductsController@edit');
Route::post('updateproduct/{id}','ProductsController@update');
Route::get('deleteproduct/{id}','ProductsController@delete');

//product_images routes
Route::get('productimages/{id}','ProductImagesController@index');
Route::post('add_more_images/{id}','ProductImagesController@add_more_images');
Route::get('deleteimage/{id}','ProductImagesController@destroy');


