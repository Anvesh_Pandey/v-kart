<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
    'client_id' => '507134955317-17opiulgsuii48kp87d5da6rtckbi89l.apps.googleusercontent.com',
    'client_secret' => 'RoN8aY-il7bJFDjZwo8wFKvF',
    'redirect' => 'http://localhost:8000/login/google/callback',
    ],

    'facebook' => [
    'client_id' => '1737549956400450',
    'client_secret' => 'c7f9ef0c293ca46857d07866292ed7e3',
    'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],

];
