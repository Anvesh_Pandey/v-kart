<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable = [
        'product_name', 'category_id','description','price','discounted_price','quantity',
    ];

    public function getCategory(){
    	return $this->belongsTo(Category::class,'category_id')->with('parentcategory');
    }
    public function getImages(){
    	return $this->hasMany(ProductImage::class,'product_id');
    }
    
}
