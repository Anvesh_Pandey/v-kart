<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   
use AuthenticatesUsers;


protected function authenticated(Request $request, $user)
{

 return redirect('/');
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    /**
     * Redirect the user to the service authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from service.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($service)
    {
        $serviceuser = Socialite::driver($service)->stateless()->user();
        //if user already exist
        $user = User::Where('email', $serviceuser->getEmail())->first();

        //if the user is new
        if(!$user){

        //add user to database
        $user =User::create([
            'email'=>$serviceuser->getEmail(),
            'name'=>$serviceuser->getName(),
            'providers_id'=>$serviceuser->getId(),
            'password' => bcrypt('secret')
        ]);

        }
        //login the user
        Auth::Login($user,true);

        //redirecting

        return redirect('/');
    }
        
}
