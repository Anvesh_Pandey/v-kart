<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
// import the storage facade
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;
use App\ProductImage;
class ProductsController extends Controller
{
    public function index()
    {	 
    	
    	$products = Product::OrderBy('created_at','desc')->with('getcategory')->with('getimages')->get();

    	return view('Admin.products.index')->with('products',$products);
    }

    public function create()
    {
    	$categories = Category::whereNull('category_id')
      	->with('subcategories')
      	->get();
    	return view('Admin.products.add_product')->with('categories',$categories);
    }
    public function store(Request $request)
    {   
         if ($request->hasFile('image'))
         {
         $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename= $request->image->getClientOriginalName();
            $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
            $store= $request->image->storeAs('images',$name,'public');

    		$product_data = [
    		'product_name' => $request->input('product_name'),
    		'category_id' => $request->input('subcategory_id'),
    		'description' => $request->input('description'),
    		'price'=> $request->input('price'),
    		'discounted_price'=> $request->input('discounted_price'),
    		'quantity'=> $request->input('quantity'),
    		 ];

		  	$data = Product::create($product_data);

            $productimage_data = [
                'product_image'=>$name,
                'product_id' => $data->id,
            ];
            ProductImage::create($productimage_data);
    		
        }
            else{
    				return redirect('/add_product')->with('error','Upload Image');
    			}
    		 return redirect('/products')->with('success','Product Added Successfully');
    }

    public function edit($id)
    {
    	$product = Product::find($id);
    	$categories = Category::whereNull('category_id')
      	->with('subcategories')
      	->get();
    	return view('Admin.products.edit')->with('product',$product)->with('categories',$categories);
    }

    public function update(Request $request, $id)
    {
    	Product::where('id',$id)->update([
    		'product_name' => $request->input('product_name'),
    		'category_id' => $request->input('subcategory_id'),
    		'description' => $request->input('description'),
    		'price'=> $request->input('price'),
    		'discounted_price'=> $request->input('discounted_price'),
    		'quantity'=> $request->input('quantity'),
    	]);

    	return redirect('/products')->with('success','Product Updated');
    } 

       protected function deleteOldimage($id)
   {
        $productImage= ProductImage::Find($id);
     if ($productImage['product_image']) 
        {
                Storage::delete('public/images/'.$productImage['product_image']);           
        }
   }

    public function delete($id)
    {          
            $products = Product::where('id',$id)->with('getimages')->get();

                foreach($products as $product)
                {
                    foreach ($product->getimages as $image) 
                    {
                       
                        $this->deleteOldimage($image->id);
                        ProductImage::destroy($image->id);
                    }
                }
    	    Product::destroy($id);
    	    return redirect()->back()->with('success','Product Deleted');
    }
}
