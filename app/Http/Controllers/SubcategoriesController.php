<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
// import the storage facade
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;
class SubcategoriesController extends Controller
{
    public function index()
    {
   		$categories = Category::whereNull('category_id')
      ->with('subcategories')
      ->get();
        return view('Admin.sub_categories.index')->with('categories',$categories);

    }

   	public function create()
   	{ 
      $categories = Category::whereNull('category_id')
      ->get();
   	  return view('Admin.sub_categories.create')->with('categories',$categories);
   	}
   	public function store(Request $request )
   	{
      if ($request->hasFile('image')){
   		 $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
    		$filename= $request->image->getClientOriginalName();
            $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
    		$store= $request->image->storeAs('images',$name,'public');
    		
    		$subcategory = Category::where('category_name', '=', $request->input('category_name'))->first();
    		if($subcategory== null)
    		{
    		$subcategory_data = [
    		'category_name' => $request->input('subcategory_name'),
    		'category_image' => $name,
    		'category_id' => $request->input('category_id'),
    		];
    		
		  	Category::create($subcategory_data);
    		}
        else
        {
          return redirect('/createsubcategories')->with('message','SubCategory Already Exist');
        }

      }
    
    	else{
    				return redirect('/createsubcategories')->with('error','image Required');
    			}


    		 return redirect('/subcategories')->with('success','SubCategory Created Successfully');;
    		

   	}

   	public function edit($id)
   	{		
		  $subcategory= Category::find($id);
       $categories = Category::whereNull('category_id')
      ->with('subcategories')
      ->get();
   		return view('Admin.sub_categories.edit')->with('subcategory',$subcategory)->with('categories',$categories);
   	}

   	public function update(Request $request , $id)
   	{	
   		
             if ($request->hasFile('image')) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $filename= $request->image->getClientOriginalName();
                    $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
                    $this->deleteOldimage($id);
                    $store= $request->image->storeAs('images',$name,'public');
                     
                     }
                    else
                    {
                         $categories= Category::Find($id);
                         $name =$categories['category_image'];
                    }

                    
               Category::where('id',$id)->
                update(['category_name'=>$request->input('subcategory_name'),
                  'category_image'=>$name,
                  'category_id' => $request->input('category_id'),
                ]);  
                   return redirect('/subcategories')->with('success','Subcategory Updated');
   	}


   	protected function deleteOldimage($id)
   {
        $subcategories= Category::Find($id);
       if ($subcategories['category_image']) 
        {
                Storage::delete('public/images/'.$subcategories['category_image']);           
        }
   }

    public function destroy($id)
    {       
    	   $product = Product::where('category_id',$id)->get()->first();
        if($product==null)
        {
            $this->deleteOldimage($id);
            Category::destroy($id);
            return redirect()->back()->with('success','SubCategory Deleted');
        }
        else 
        {
            return redirect()->back()->with('error','Can\'t delete Products Exist ');
        }
    }


}