<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
// import the storage facade
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;
use App\ProductImage;
use Illuminate\Support\Facades\Validator;
class ProductImagesController extends Controller
{
    public function index($id)
    {
    	$products = Product::where('id',$id)->with('getimages')->get();
    	 return view('Admin.product_images.index')->with('products',$products);
    }

    public function add_more_images(Request $request ,$id)
    {
    	$product_count = ProductImage::where('product_id',$id)->get();

    	if (count($product_count) < 5 ) 
    	{	
    	$request->validate( 
  				  [
   			 	'images' => 'required|array', 
    			'images.*' => 'mimes:jpeg,jpg,png',
 			  ]);

        foreach ($request->images as $image) {
    		 $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
    		 $filename= $image->getClientOriginalName();
             $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
    		 $store= $image->storeAs('images',$name,'public');

        	ProductImage::create([
            'product_id' => $id,
            'product_image' => $name,
        	]);

        	}
        	return redirect()->back()->with('success','Uploaded');
        }
        else
        {
        	return redirect()->back()->with('error','Max. 5 Images could be Upload for Products');
        }
    }

    protected function deleteOldimage($id)
   {
        $productImage= ProductImage::Find($id);
     if ($productImage['product_image']) 
        {
                Storage::delete('public/images/'.$productImage['product_image']);           
        }
   }
     public function destroy($id )
    {      

             $this->deleteOldimage($id);
             ProductImage::destroy($id);
         
             return redirect()->back()->with('success','image deleted');
             
    }
    
}
