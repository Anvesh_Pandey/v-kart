<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
// import the storage facade
use Illuminate\Support\Facades\Storage;
use App\Category;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CategoryExport;
class CategoriesController extends Controller
{   

    public function index(){
         $categories = Category::whereNull('category_id')
        ->get();
        return view('Admin.categories.index')->with('categories',$categories);

    }

    public function create()
    {
        return view('Admin.categories.create');   
    }

    public function store(Request $request)
    {	    

      if ($request->hasFile('image')){
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
    		$filename= $request->image->getClientOriginalName();
        $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
    		$store= $request->image->storeAs('images',$name,'public');
    		
    		$category = Category::where('category_name', '=', $request->input('category_name'))->first();
    		if($category== null)
    		{
    		$category_data = [
    		'category_name' => $request->input('category_name'),
    		'category_image' => $name,
        'category_id' => NULL,
    		];

    		Category::create($category_data);
    		
    		  }
          else
          {
            return redirect('/createcategory')->with('message','Category Already Exist');
          }

        }
    		else
    		{
    			return redirect('/createcategory')->with('error','image Required');
    		}
			
    		return redirect('/categories')->with('success','Category Created Successfully');

    }

 	 public function edit($id)
  		{
  			$categories = Category::Find($id);

  			return view('Admin.categories.edit')->with('categories',$categories);  
 		}
 	public function update(Request $request , $id)
 	{         
          
                 if ($request->hasFile('image')) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $filename= $request->image->getClientOriginalName();
                    $name = $timestamp .'-' . str_replace([' ', ':'], '-',$filename);
                    $this->deleteOldimage($id);
                    $store= $request->image->storeAs('images',$name,'public');
                     
                     }
                    else
                    {
                         $categories= Category::Find($id);
                         $name =$categories['category_image'];
                    }

                    
               Category::where('id',$id)->
                update(['category_name'=>$request->input('category_name'),
                  'category_image'=>$name]);  
               return redirect('/categories')->with('success','Category updated Successfully');
                    
   }

   protected function deleteOldimage($id)
   {
        $categories= Category::Find($id);
     if ($categories['category_image']) 
        {
                Storage::delete('public/images/'.$categories['category_image']);           
        }
   }
    public function destroy($id)
    {       
        $subcategory = Category::where('category_id',$id)->get()->first();
          if($subcategory==null)
          {

             $this->deleteOldimage($id);
             Category::destroy($id);
             return redirect()->back()->with('success','Category deleted Successfully');
          }
          else
          {
             return redirect()->back()->with('error','Can\'t Delete Subcategories Exist');
          }
    }

    public function export()
    {
      return Excel::download(new CategoryExport,'categories.xlsx');
    }
}
