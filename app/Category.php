<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
	protected $fillable = [
        'category_name', 'category_image','category_id',
    ];
    public function categories()
    {
    	return $this->hasMany(Category::class);	
    }

    public function subCategories()
    {
    	return $this->hasMany(Category::class)->with('categories');
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}
 